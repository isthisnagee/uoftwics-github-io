# uoftwics.github.io

U of T Women in Computer Science Website

## SETUP

Be sure to install [hugo](https://gohugo.io/)!

```bash
git submodule init
git submodule update
hugo server --disableFastRender
```

## RUN
```bash
hugo server -D
```
